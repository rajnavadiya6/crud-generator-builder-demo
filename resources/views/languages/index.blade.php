@extends('layouts.app')
@section('title')
    Languages
@endsection
@push('css')
    <style>
        .tab-pane.fade {
            transition: all 0.2s;
            transform: translateY(1rem);
        }

        .tab-pane.fade.show {
            transform: translateY(0rem);
        }
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Languages</h1>
            <div class="section-header-breadcrumb">
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('language.update') }}" method="POST">
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="nav nav-pills flex-column" id="languesTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active show" id="home-tab4" data-toggle="tab" href="#single"
                                           role="tab">Single</a>
                                    </li>
                                    @foreach($groupLanguages as $key => $language)
                                        <li class="nav-item mt-2">
                                            <a class="nav-link " id="home-tab4" data-toggle="tab"
                                               href="#{{$key}}" role="tab">{{ ucwords($key,'_') }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content" id="languesTab">
                                    <div class="tab-pane fade active show" id="single" role="tabpanel">
                                        <div class="row">
                                            @foreach($singleLanguages as $key => $language)
                                                <div class="form-group col-md-4">
                                                    <label for="{{$key}}">
                                                        {{ str_replace('_',' ',ucwords($key,'_')) }}:</label>
                                                    <input type="text" name="{{$key}}" class="form-control"
                                                           value="{{$language }}"
                                                           required>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @foreach($groupLanguages as $key => $languages)
                                        <div class="tab-pane fade" id="{{$key}}" role="tabpanel">
                                            <div class="row">
                                                @foreach($languages as $childKey => $language)
                                                    <div class="form-group col-md-4">
                                                        <label for="{{$childKey}}">
                                                            {{ str_replace('_',' ',ucwords($childKey,'_')) }}
                                                            :</label>
                                                        <input type="text" name="{{$key}}[{{$childKey}}]"
                                                               class="form-control"
                                                               value="{{$language}}"
                                                               required>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')

@endpush
