<li class="side-menus {{ Request::is('dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('dashboard') }}">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>
<li class="side-menus {{ Request::is('posts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('posts.index') }}">
        <i class=" fas fa-building"></i><span>Posts</span>
    </a>
</li>
<li class="side-menus {{ Request::is('demos*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('demos.index') }}">
        <i class="fas fa-building"></i><span>Demos</span>
    </a>
</li>
<li class="side-menus {{ Request::is('demo2s*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('demo2s.index') }}">
        <i class="fas fa-building"></i><span>Demo2S</span>
    </a>
</li>
<li class="side-menus {{ Request::is('manage-languages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('language.index') }}">
        <i class="fas fa-language"></i><span>Language</span>
    </a>
</li>
<li class="side-menus {{ Request::is('simple*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('io_generator_builder') }}">
        <i class="fas fa-building"></i><span>Generator Builder</span>
    </a>
</li>
<li class="side-menus {{ Request::is('settings*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('get.settings') }}">
        <i class="fas fa-sliders-h"></i><span>Settings</span>
    </a>
</li>

