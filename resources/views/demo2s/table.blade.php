<table class="table table-responsive-sm table-striped table-bordered" id="demo2stbl">
    <thead>
    <tr>
        <th>Title</th>
        <th>Body</th>
        <th>Email</th>
        <th>Author Gender</th>
        <th>Post Type</th>
        <th>Post Visits</th>
        <th>Category</th>
        <th>Category Short</th>
        <th>Post Date</th>
        <th>Is Private</th>
        <th scope="col" class="action-column">Action</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
