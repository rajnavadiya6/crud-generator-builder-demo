<div id="createModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Demo2</h5>
                <button type="button" aria-label="Close" class="close" data-dismiss="modal">×</button>
            </div>
            {!! Form::open(['route' => 'demo2s.store','id'=>'createForm']) !!}
            <div class="modal-body">
                <div class="row">
                    @include('demo2s.fields')
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
