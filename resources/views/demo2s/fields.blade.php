<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control '. ($errors->has('title') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
</div>


<!-- Body Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('body', 'Body:') !!}
    {!! Form::textarea('body', old('body'), ['class' => 'form-control '. ($errors->has('body') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('body') }}</div>
</div>


<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control '. ($errors->has('email') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
</div>


<!-- Author Gender Field -->
<div class="form-group col-sm-12">
    {!! Form::label('author_gender', 'Author Gender:') !!}
    <label class="radio-inline">
        {!! Form::radio('author_gender', "1", null) !!} Male
    </label>

    <label class="radio-inline">
        {!! Form::radio('author_gender', "0", null) !!} Female
    </label>

    <small class="d-block text-danger">{{ $errors->first('author_gender') }}</small>
</div>


<!-- Post Type Field -->
<div class="form-group col-sm-12">
    {!! Form::label('post_type', 'Post Type:') !!}
    <label class="radio-inline">
        {!! Form::radio('post_type', "Public", null) !!} Public
    </label>

    <label class="radio-inline">
        {!! Form::radio('post_type', "Private", null) !!} Private
    </label>

    <small class="d-block text-danger">{{ $errors->first('post_type') }}</small>
</div>


<!-- Post Visits Field -->
<div class="form-group col-sm-6">
    {!! Form::label('post_visits', 'Post Visits:') !!}
    {!! Form::number('post_visits', null, ['class' => 'form-control '. ($errors->has('post_visits') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('post_visits') }}</div>
</div>


<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::select('category', ['Technology' => 'Technology', 'LifeStyle' => 'LifeStyle', 'Education' => 'Education', 'Games' => 'Games'], null, ['class' => 'form-control '. ($errors->has('category') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('category') }}</div>
</div>


<!-- Category Short Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_short', 'Category Short:') !!}
    {!! Form::select('category_short', ['' => 'Select', 'tech' => 'Technology', 'ls' => 'LifeStyle', 'edu' => 'Education', 'game' => 'Games'], null, ['id' => '','class' => 'form-control custom_select '. ($errors->has('category_short') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('category_short') }}</div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.custom_select').select2({
                width: '100%',
            });
        });
    </script>
@endpush


<!-- Post Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('post_date', 'Post Date:') !!}
    {!! Form::text('post_date', null, ['class' => 'custom_datepicker form-control','id'=>'post_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('.custom_datepicker').datetimepicker(DatetimepickerDefaults({
            format: 'YYYY-MM-DD',
            useCurrent: true,
            sideBySide: true,
        }));
    </script>
@endpush


<!-- Is Private Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_private', 'Is Private:') !!}
    <label class="checkbox-inline {{  ($errors->has('is_private') ? 'is-invalid':'') }}">
        {!! Form::hidden('is_private', 0) !!}
        {!! Form::checkbox('is_private', '1', null) !!}
    </label>
    <div class="invalid-feedback">{{ $errors->first('is_private') }}</div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('demo2s.index') }}" class="btn btn-light">Cancel</a>
</div>
