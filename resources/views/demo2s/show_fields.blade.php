<!-- Title Field -->
<div class="form-group col-md-6">
    {!! Form::label('title', 'Title:',['class' => 'font-weight-bold']) !!}
    <p id="show_title"></p>
</div>


<!-- Body Field -->
<div class="form-group col-md-6">
    {!! Form::label('body', 'Body:',['class' => 'font-weight-bold']) !!}
    <p id="show_body"></p>
</div>


<!-- Email Field -->
<div class="form-group col-md-6">
    {!! Form::label('email', 'Email:',['class' => 'font-weight-bold']) !!}
    <p id="show_email"></p>
</div>


<!-- Author Gender Field -->
<div class="form-group col-md-6">
    {!! Form::label('author_gender', 'Author Gender:',['class' => 'font-weight-bold']) !!}
    <p id="show_author_gender"></p>
</div>


<!-- Post Type Field -->
<div class="form-group col-md-6">
    {!! Form::label('post_type', 'Post Type:',['class' => 'font-weight-bold']) !!}
    <p id="show_post_type"></p>
</div>


<!-- Post Visits Field -->
<div class="form-group col-md-6">
    {!! Form::label('post_visits', 'Post Visits:',['class' => 'font-weight-bold']) !!}
    <p id="show_post_visits"></p>
</div>


<!-- Category Field -->
<div class="form-group col-md-6">
    {!! Form::label('category', 'Category:',['class' => 'font-weight-bold']) !!}
    <p id="show_category"></p>
</div>


<!-- Category Short Field -->
<div class="form-group col-md-6">
    {!! Form::label('category_short', 'Category Short:',['class' => 'font-weight-bold']) !!}
    <p id="show_category_short"></p>
</div>


<!-- Post Date Field -->
<div class="form-group col-md-6">
    {!! Form::label('post_date', 'Post Date:',['class' => 'font-weight-bold']) !!}
    <p id="show_post_date"></p>
</div>


<!-- Is Private Field -->
<div class="form-group col-md-6">
    {!! Form::label('is_private', 'Is Private:',['class' => 'font-weight-bold']) !!}
    <p id="show_is_private"></p>
</div>


<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:',['class' => 'font-weight-bold']) !!}
    <p id="show_created_at"></p>
</div>


<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:',['class' => 'font-weight-bold']) !!}
    <p id="show_updated_at"></p>
</div>


