@extends('layouts.app')
@section('title')
    Demo2S
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/summernote.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Demo2S</h1>
            <div class="section-header-breadcrumb">
                <a class="btn btn-primary" data-toggle="modal" data-target="#createModal">Demo2 <i
                            class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('demo2s.table')
                </div>
            </div>
        </div>
    </section>
    @include('demo2s.create')
    @include('demo2s.edit')
    @include('demo2s.show')
@endsection
@push('scripts')
    <script>
        let indexUrl = "{{ route('demo2s.index') }}/";
        let tableName = '#demo2stbl';
        let createFormId = '#createForm';
        let editFormId = '#editForm';
        let createModelId = '#createModal';
        let editModelId = '#editModal';
        let showModelId = '#showModal';
        let editButtonSelector = '.edit-btn';
        let deleteButtonSelector = '.delete-btn';
        let showButtonSelector = '.show-btn';
    </script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ mix('assets/js/custom/custom-datatable.js') }}"></script>
    <script src="{{ mix('assets/js/custom/common-crud.js')}}"></script>
@endpush
