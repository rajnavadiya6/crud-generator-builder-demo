<table class="table table-responsive" id="demos-table">
    <thead>
    <tr>
        <th>Title</th>
        <th>Body</th>
        <th>Email</th>
        <th>Author Gender</th>
        <th>Post Type</th>
        <th>Post Visits</th>
        <th>Category</th>
        <th>Category Short</th>
        <th>Is Private</th>
        <th class="action-column">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($demos as $demo)
        <tr>
            <td>{{ $demo->title }}</td>
            <td>{{ $demo->body }}</td>
            <td>{{ $demo->email }}</td>
            <td>{{ $demo->author_gender }}</td>
            <td>{{ $demo->post_type }}</td>
            <td>{{ $demo->post_visits }}</td>
            <td>{{ $demo->category }}</td>
            <td>{{ $demo->category_short }}</td>
            <td>{{ $demo->is_private }}</td>
            <td class="text-center">
                {!! Form::open(['route' => ['demos.destroy', $demo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('demos.show', [$demo->id]) !!}" class='btn btn-light action-btn '><i
                                class="fa fa-eye"></i></a>
                    <a href="{!! route('demos.edit', [$demo->id]) !!}" class='btn btn-warning action-btn edit-btn'><i
                                class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

