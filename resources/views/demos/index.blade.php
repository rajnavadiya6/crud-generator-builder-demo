@extends('layouts.app')
@section('title')
    Demos
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Demos</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('demos.create')}}" class="btn btn-primary form-btn">Demo <i
                            class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('demos.table')
                </div>
            </div>
        </div>

    </section>
@endsection
