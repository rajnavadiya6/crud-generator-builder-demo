@extends('layouts.app')
@section('title')
    Demo Details
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Demo Details</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('demos.index') }}"
                   class="btn btn-primary form-btn float-right">Back</a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('demos.show_fields')
                </div>
            </div>
        </div>
    </section>
@endsection
