@extends('layouts.app')
@section('title')
    Create Demo
@endsection
@push('css')
    <link href="{{ asset('css/dropify.min.css') }}" rel="stylesheet"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">New Demo</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="{{ route('demos.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
        <div class="content">
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body ">
                                {!! Form::open(['route' => 'demos.store','class'=>'','files'=> true]) !!}
                                <div class="row">
                                    @include('demos.fields')
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script src="{{ asset('js/dropify.min.js') }}"></script>

    <script>
        $('.dropify').dropify();
    </script>
@endpush
