<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $demo->title }}</p>
</div>


<!-- Body Field -->
<div class="form-group">
    {!! Form::label('body', 'Body:') !!}
    <p>{{ $demo->body }}</p>
</div>


<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $demo->email }}</p>
</div>


<!-- Author Gender Field -->
<div class="form-group">
    {!! Form::label('author_gender', 'Author Gender:') !!}
    <p>{{ $demo->author_gender }}</p>
</div>


<!-- Post Type Field -->
<div class="form-group">
    {!! Form::label('post_type', 'Post Type:') !!}
    <p>{{ $demo->post_type }}</p>
</div>


<!-- Post Visits Field -->
<div class="form-group">
    {!! Form::label('post_visits', 'Post Visits:') !!}
    <p>{{ $demo->post_visits }}</p>
</div>


<!-- Category Field -->
<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    <p>{{ $demo->category }}</p>
</div>


<!-- Category Short Field -->
<div class="form-group">
    {!! Form::label('category_short', 'Category Short:') !!}
    <p>{{ $demo->category_short }}</p>
</div>


<!-- Is Private Field -->
<div class="form-group">
    {!! Form::label('is_private', 'Is Private:') !!}
    <p>{{ $demo->is_private }}</p>
</div>


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $demo->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $demo->updated_at }}</p>
</div>


