<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control '. ($errors->has('name') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
</div>


<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', old('description'), ['class' => 'form-control '. ($errors->has('name') ? 'is-invalid':'')]) !!}
    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('posts.index') }}" class="btn btn-light">Cancel</a>
</div>
