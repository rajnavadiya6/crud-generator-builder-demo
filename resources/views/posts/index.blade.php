@extends('layouts.app')
@section('title')
    Posts
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/summernote.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Posts</h1>
            <div class="section-header-breadcrumb">
                <a class="btn btn-primary" data-toggle="modal" data-target="#createModal">Post <i
                            class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('posts.table')
                </div>
            </div>
        </div>
    </section>
    @include('posts.create')
    @include('posts.edit')
    @include('posts.show')
@endsection
@push('scripts')
    <script>
        let indexUrl = "{{ route('posts.index') }}/";
        let tableName = '#poststbl';
        let createFormId = '#createForm';
        let editFormId = '#editForm';
        let createModelId = '#createModal';
        let editModelId = '#editModal';
        let showModelId = '#showModal';
        let editButtonSelector = '.edit-btn';
        let deleteButtonSelector = '.delete-btn';
        let showButtonSelector = '.show-btn';
    </script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ mix('assets/js/custom/custom-datatable.js') }}"></script>
    <script src="{{ mix('assets/js/custom/common-crud.js')}}"></script>
@endpush
