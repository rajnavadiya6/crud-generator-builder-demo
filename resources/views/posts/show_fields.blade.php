<!-- Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('name', 'Name:',['class' => 'font-weight-bold']) !!}
    <p id="show_name"></p>
</div>


<!-- Description Field -->
<div class="form-group col-md-6">
    {!! Form::label('description', 'Description:',['class' => 'font-weight-bold']) !!}
    <p id="show_description"></p>
</div>


<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:',['class' => 'font-weight-bold']) !!}
    <p id="show_created_at"></p>
</div>


<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:',['class' => 'font-weight-bold']) !!}
    <p id="show_updated_at"></p>
</div>


