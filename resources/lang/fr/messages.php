<?php

return [

    /*
    |--------------------------------------------------------------------------
    | All Titles and static string in blade files
    |--------------------------------------------------------------------------
    |
    */
    //menu.blade keys
    'dashboard'      => 'Tableau de bord',
    'pricings_table' => 'Tableau des prix',

    'common' => [
        'save'    => 'sauver',
        'cancel'  => 'Annuler',
        'edit'    => 'Éditer',
        'delete'  => 'Supprimer',
        'preview' => 'Aperçu',
    ],
];
