<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::resource('posts', App\Http\Controllers\PostController::class);

Route::resource('demos', App\Http\Controllers\DemoController::class);

Route::get('manage-languages', [\App\Http\Controllers\DemoController::class, 'getLanguages'])->name('language.index');
Route::post('update-languages', [\App\Http\Controllers\DemoController::class, 'setLanguages'])->name('language.update');
Route::get('settings', [\App\Http\Controllers\DemoController::class, 'getSettings'])->name('get.settings');
Route::resource('demo2s', App\Http\Controllers\Demo2Controller::class);


Route::group(['prefix' => 'crud-gui'], function () {
    Route::get('generator_builder',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

    Route::get('field_template',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

    Route::get('relation_field_template',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

    Route::post('generator_builder/generate',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

    Route::post('generator_builder/rollback',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

    Route::post(
        'generator_builder/generate-from-file',
        '\Crud\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
    )->name('io_generator_builder_generate_from_file');
});
