/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/custom/common-crud.js":
/*!***************************************************!*\
  !*** ./resources/assets/js/custom/common-crud.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


$(tableName).DataTable({
  ajax: {
    url: indexUrl
  }
});
$(document).on('submit', createFormId, function (e) {
  var formId = '#' + $(this).attr('id');
  e.preventDefault();
  processingBtn(formId, '#btnSave', 'loading');
  var url = $(this).attr('action');
  var data = $(this).serialize();
  submitForm(url, 'POST', data, formId, createModelId, tableName);
});
$(document).on('submit', editFormId, function (e) {
  e.preventDefault();
  var formId = '#' + $(this).attr('id');
  processingBtn(formId, '#btnSave', 'loading');
  var id = $('#editId').val();
  var url = indexUrl + id;
  var data = $(this).serialize();
  submitForm(url, 'PUT', data, formId, editModelId, tableName);
});
$(document).on('click', editButtonSelector, function (event) {
  var id = $(event.currentTarget).data('id');
  renderData(id);
});

window.renderData = function (id) {
  $.ajax({
    url: indexUrl + id + '/edit',
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        var data = result.data;
        $('#editId').val(data.id);
        fillEditDataWithKeys(data, editFormId);
        $(editModelId).appendTo('body').modal('show');
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    }
  });
};

$(document).on('click', showButtonSelector, function (event) {
  var id = $(event.currentTarget).data('id');
  $.ajax({
    url: indexUrl + id,
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        var data = result.data;
        showModelData(data);
        $(showModelId).appendTo('body').modal('show');
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    }
  });
});
$(document).on('click', deleteButtonSelector, function (event) {
  var Id = $(event.currentTarget).data('id');
  var deleteUrl = indexUrl + Id;
  deleteItem(deleteUrl, tableName, 'Demo');
});
$(createModelId).on('hidden.bs.modal', function () {
  resetModalForm(createFormId);
});
$(editModelId).on('hidden.bs.modal', function () {
  resetModalForm(editFormId);
});

/***/ }),

/***/ 3:
/*!*********************************************************!*\
  !*** multi ./resources/assets/js/custom/common-crud.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\wamp64\www\blog\resources\assets\js\custom\common-crud.js */
    './resources/assets/js/custom/common-crud.js');


/***/ })

/******/ });
