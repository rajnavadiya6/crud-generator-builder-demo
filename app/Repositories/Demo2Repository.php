<?php

namespace App\Repositories;

use App\Models\Demo2;
use App\Repositories\BaseRepository;

/**
 * Class Demo2Repository
 * @package App\Repositories
 * @version December 9, 2020, 5:23 pm UTC
 */
class Demo2Repository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'email',
        'post_type',
        'category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Demo2::class;
    }
}
