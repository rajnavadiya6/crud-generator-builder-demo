<?php

namespace App\Repositories;

use App\Models\Demo;
use App\Repositories\BaseRepository;

/**
 * Class DemoRepository
 * @package App\Repositories
 * @version December 5, 2020, 1:59 pm UTC
 */
class DemoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'email',
        'post_type',
        'category',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Demo::class;
    }
}
