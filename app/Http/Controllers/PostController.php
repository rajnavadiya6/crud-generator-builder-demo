<?php

namespace App\Http\Controllers;

use App\DataTables\PostDataTable;
use App\Models\Post;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repositories\PostRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\View;
use Exception;
use Response;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
    }

    /**
     * Display a listing of the Post.
     *
     * @param  Request  $request
     *
     * @return View|Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data['data'] = (new PostDataTable())->get();

            return $data;
        }

        return view('posts.index');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param  CreatePostRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();

        $post = $this->postRepository->create($input);

        return $this->sendSuccess('Post saved successfully.');
    }

    /**
     * Display the specified Post.
     *
     * @param  Post  $post
     *
     * @return JsonResponse
     */
    public function show(Post $post)
    {
        return $this->sendResponse($post, 'Post Retrieved Successfully.');
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  Post  $post
     *
     * @return JsonResponse
     */
    public function edit(Post $post)
    {
        return $this->sendResponse($post, 'Post Retrieved Successfully.');
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  UpdatePostRequest  $request
     * @param  Post  $post
     *
     * @return JsonResponse
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $this->postRepository->update($request->all(), $post->id);

        return $this->sendSuccess('Post updated successfully.');
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  Post  $post
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return $this->sendSuccess('Post deleted successfully.');
    }
}
