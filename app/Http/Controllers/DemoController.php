<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDemoRequest;
use App\Http\Requests\UpdateDemoRequest;
use App\Repositories\DemoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Response;

class DemoController extends AppBaseController
{
    /** @var  DemoRepository */
    private $demoRepository;

    public function __construct(DemoRepository $demoRepo)
    {
        $this->demoRepository = $demoRepo;
    }

    /**
     * Display a listing of the Demo.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $demos = $this->demoRepository->all();

        return view('demos.index')
            ->with('demos', $demos);
    }

    /**
     * Show the form for creating a new Demo.
     *
     * @return Response
     */
    public function create()
    {
        return view('demos.create');
    }

    /**
     * Store a newly created Demo in storage.
     *
     * @param  CreateDemoRequest  $request
     *
     * @return Response
     */
    public function store(CreateDemoRequest $request)
    {
        $input = $request->all();

        $demo = $this->demoRepository->create($input);

        Flash::success('Demo saved successfully.');

        return redirect(route('demos.index'));
    }

    /**
     * Display the specified Demo.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $demo = $this->demoRepository->find($id);

        if (empty($demo)) {
            Flash::error('Demo not found');

            return redirect(route('demos.index'));
        }

        return view('demos.show')->with('demo', $demo);
    }

    /**
     * Show the form for editing the specified Demo.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $demo = $this->demoRepository->find($id);

        if (empty($demo)) {
            Flash::error('Demo not found');

            return redirect(route('demos.index'));
        }

        return view('demos.edit')->with('demo', $demo);
    }

    /**
     * Update the specified Demo in storage.
     *
     * @param  int  $id
     * @param  UpdateDemoRequest  $request
     *
     * @return Response
     */
    public function update($id, UpdateDemoRequest $request)
    {
        $demo = $this->demoRepository->find($id);

        if (empty($demo)) {
            Flash::error('Demo not found');

            return redirect(route('demos.index'));
        }

        $demo = $this->demoRepository->update($request->all(), $id);

        Flash::success('Demo updated successfully.');

        return redirect(route('demos.index'));
    }

    /**
     * Remove the specified Demo from storage.
     *
     * @param  int  $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $demo = $this->demoRepository->find($id);

        if (empty($demo)) {
            Flash::error('Demo not found');

            return redirect(route('demos.index'));
        }

        $this->demoRepository->delete($id);

        Flash::success('Demo deleted successfully.');

        return redirect(route('demos.index'));
    }

    public function getLanguages()
    {
        $languages = Lang::get('messages');
        $groupLanguages = [];
        $singleLanguages = [];
        foreach ($languages as $key => $language){
            if(is_array($language)){
                $groupLanguages[$key] = $language;
            }else{
                $singleLanguages[$key] = $language;
            }
        }

        return view('languages.index', compact('groupLanguages','singleLanguages'));
    }

//    public function setLanguages(Request $request)
//    {
//        $data = $request->except('_token');
//        file_put_contents(resource_path('lang/en/messages.php'), "<?php \n\nreturn [\n");
//        foreach ($data as $key => $value) {
//            if (is_array($value)) {
//                file_put_contents(resource_path('lang/en/messages.php'), "\n\t'$key' => [ \n", FILE_APPEND);
//                foreach ($value as $childkey => $childval) {
//                    $childval = str_replace(array("'", "\"", "&quot;"), "\'", htmlspecialchars($childval));
//                    file_put_contents(resource_path('lang/en/messages.php'), "\t\t '$childkey' => '$childval', \n",
//                        FILE_APPEND);
//                }
//                file_put_contents(resource_path('lang/en/messages.php'), "\t], \n", FILE_APPEND);
//            } else {
//                $value = str_replace(array("'", "\"", "&quot;"), "\'", htmlspecialchars($value));
//                file_put_contents(resource_path('lang/en/messages.php'), "\t'$key' => '$value', \n", FILE_APPEND);
//            }
//        }
/*        file_put_contents(resource_path('lang/en/messages.php'), "\n]  \n?>", FILE_APPEND);*/
//
//        return redirect(route('language.index'));
//    }

    public function setLanguages(Request $request)
    {
        $data = $request->except('_token');
        $fileData = '<?php return '.var_export($data, true).'?>';
        $file = resource_path('lang/en/messages.php');
        file_put_contents($file, $fileData);

        return redirect(route('language.index'));
    }

    public function getSettings(Request $request)
    {
        return view('settings.index');
    }


}
