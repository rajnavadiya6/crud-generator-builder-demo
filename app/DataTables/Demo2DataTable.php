<?php

namespace App\DataTables;

use App\Models\Demo2;

class Demo2DataTable
{
    public function get()
    {
        $model = new Demo2();
        /** @var Post $query */
        $query = Demo2::all();
        $query->map(function ($item) {
            $id = $item['id'];
            $item['action'] = view('demo2s.datatables_actions', compact('id'))->render();

            return $item;
        });
        $field = $model->getFillable();
        array_push($field, 'action');
        $data = $query->map->only($field)->toArray();

        return array_map('array_values', $data);
    }
}
