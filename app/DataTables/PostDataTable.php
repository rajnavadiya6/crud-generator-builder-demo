<?php

namespace App\DataTables;

use App\Models\Post;

class PostDataTable
{
    public function get()
    {
        $model = new Post();
        /** @var Post $query */
        $query = Post::all();
        $query->map(function ($item) {
            $id = $item['id'];
            $item['action'] = view('posts.datatables_actions', compact('id'))->render();

            return $item;
        });
        $field = $model->getFillable();
        array_push($field, 'action');
        $data = $query->map->only($field)->toArray();

        return array_map('array_values', $data);
    }
}
