<?php

namespace App\DataTables;

use App\Models\School;

class SchoolDataTable
{
    public function get()
    {
        $model = new School();
        /** @var Post $query */
        $query = School::all();
        $query->map(function ($item) {
            $id = $item['id'];
            $item['action'] = view('schools.datatables_actions', compact('id'))->render();

            return $item;
        });
        $field = $model->getFillable();
        array_push($field, 'action');
        $data = $query->map->only($field)->toArray();

        return array_map('array_values', $data);
    }
}
