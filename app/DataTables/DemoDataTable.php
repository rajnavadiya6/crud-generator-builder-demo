<?php

namespace App\DataTables;

use App\Models\Demo;

class DemoDataTable
{
    public function get()
    {
        $model = new Demo();
        /** @var Post $query */
        $query = Demo::all();
        $query->map(function ($item) {
            $id = $item['id'];
            $item['action'] = view('demos.datatables_actions', compact('id'))->render();

            return $item;
        });
        $field = $model->getFillable();
        array_push($field, 'action');
        $data = $query->map->only($field)->toArray();

        return array_map('array_values', $data);
    }
}
