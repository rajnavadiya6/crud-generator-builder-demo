<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Demo
 * @package App\Models
 * @version December 5, 2020, 1:59 pm UTC
 *
 * @property string $title
 * @property string $body
 * @property string $email
 * @property integer $author_gender
 * @property string $post_type
 * @property integer $post_visits
 * @property string $category
 * @property string $category_short
 * @property boolean $is_private
 */
class Demo extends Model
{

    use HasFactory;

    public $table = 'demos';


    public $fillable = [
        'title',
        'body',
        'email',
        'author_gender',
        'post_type',
        'post_visits',
        'category',
        'category_short',
        'is_private',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'             => 'integer',
        'title'          => 'string',
        'body'           => 'string',
        'email'          => 'string',
        'author_gender'  => 'integer',
        'post_type'      => 'string',
        'post_visits'    => 'integer',
        'category'       => 'string',
        'category_short' => 'string',
        'is_private'     => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'          => 'required',
        'body'           => 'required',
        'email'          => 'required',
        'author_gender'  => 'required',
        'post_type'      => 'required',
        'post_visits'    => 'required',
        'category'       => 'required',
        'category_short' => 'required',
        'is_private'     => 'required',
    ];


}
