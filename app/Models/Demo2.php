<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Demo2
 * @package App\Models
 * @version December 9, 2020, 5:23 pm UTC
 *
 * @property string $title
 * @property string $body
 * @property string $email
 * @property integer $author_gender
 * @property string $post_type
 * @property integer $post_visits
 * @property string $category
 * @property string $category_short
 * @property string $post_date
 * @property boolean $is_private
 */
class Demo2 extends Model
{

    use HasFactory;

    public $table = 'demo2s';


    public $fillable = [
        'title',
        'body',
        'email',
        'author_gender',
        'post_type',
        'post_visits',
        'category',
        'category_short',
        'post_date',
        'is_private',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'             => 'integer',
        'title'          => 'string',
        'body'           => 'string',
        'email'          => 'string',
        'author_gender'  => 'integer',
        'post_type'      => 'string',
        'post_visits'    => 'integer',
        'category'       => 'string',
        'category_short' => 'string',
        'post_date'      => 'date',
        'is_private'     => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'          => 'required',
        'body'           => 'required',
        'email'          => 'required',
        'author_gender'  => 'required',
        'post_type'      => 'required',
        'post_visits'    => 'required',
        'category'       => 'required',
        'category_short' => 'required',
        'post_date'      => 'required',
        'is_private'     => 'required',
    ];


}
