<?php

namespace Database\Factories;

use App\Models\Demo;
use Illuminate\Database\Eloquent\Factories\Factory;

class DemoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Demo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'          => $this->faker->word,
            'body'           => $this->faker->text,
            'email'          => $this->faker->word,
            'author_gender'  => $this->faker->randomDigitNotNull,
            'post_type'      => $this->faker->word,
            'post_visits'    => $this->faker->randomDigitNotNull,
            'category'       => $this->faker->word,
            'category_short' => $this->faker->word,
            'is_private'     => $this->faker->word,
            'created_at'     => $this->faker->date('Y-m-d H:i:s'),
            'updated_at'     => $this->faker->date('Y-m-d H:i:s'),
        ];
    }
}
